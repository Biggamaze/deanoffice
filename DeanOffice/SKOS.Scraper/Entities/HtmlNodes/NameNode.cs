﻿using HtmlAgilityPack;
using SKOS.Scraper.Entities.BaseEntities;

namespace SKOS.Scraper.Entities.HtmlNodes
{
    internal class NameNode : HtmlNodeBase
    {
        public NameNode(HtmlNode node) : base(node)
        {
        }
    }
}
