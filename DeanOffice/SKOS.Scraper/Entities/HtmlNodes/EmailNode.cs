﻿using HtmlAgilityPack;
using SKOS.Scraper.Entities.BaseEntities;

namespace SKOS.Scraper.Entities.HtmlNodes
{
    internal class EmailNode : HtmlNodeBase
    {
        public EmailNode(HtmlNode node) : base(node)
        {

        }
    }
}
