﻿using HtmlAgilityPack;
using System.Collections.Generic;

namespace SKOS.Scraper.Entities.BaseEntities
{
    public abstract class HtmlNodeBase 
    {
        public HtmlAttributeCollection Attributes { get; private set; }
        public string InnerHtml { get; private set; }
        public IEnumerable<HtmlNode> Descendants { get; private set; }

        public HtmlNodeBase(HtmlNode node)
        {
            Attributes = node.Attributes;
            InnerHtml = node.InnerHtml;
            Descendants = node.Descendants();
        }
    }
}
