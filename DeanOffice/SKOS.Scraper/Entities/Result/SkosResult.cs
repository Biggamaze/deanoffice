﻿namespace SKOS.Scraper.Entities.Result
{
    public class SkosResult
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }
    }
}
