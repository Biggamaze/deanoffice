﻿using HtmlAgilityPack;
using SKOS.Scraper.Entities.HtmlNodes;

namespace SKOS.Scraper.NodeSelectors
{
    internal static class NameNodeSelector
    {
        public static NameNode GetFullNameNode(this HtmlDocument document)
        {
            return new NameNode(document.GetElementbyId("fn"));
        }
    }
}
