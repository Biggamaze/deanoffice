﻿using HtmlAgilityPack;
using HtmlAgilityPack.CssSelectors.NetCore;
using SKOS.Scraper.Entities.HtmlNodes;

namespace SKOS.Scraper.NodeParsers
{
    internal static class EmailNodeSelector
    {
        public static EmailNode GetEmailNode(this HtmlDocument html)
        {
            return new EmailNode(html.QuerySelector(".email .get-object"));
        }
    }
}
