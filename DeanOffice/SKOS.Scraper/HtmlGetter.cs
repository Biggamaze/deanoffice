﻿using DeanOffice.Common.Enums;
using HtmlAgilityPack;
using HtmlAgilityPack.CssSelectors.NetCore;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Threading.Tasks;

[assembly: InternalsVisibleTo("Skos.Scraper.Tests.Integration")]
namespace SKOS.Scraper
{
    internal class HtmlGetter
    {
        private HtmlGetterSkosAccessor SkosAccessor { get; }

        public HtmlGetter()
        {
            SkosAccessor = new HtmlGetterSkosAccessor();
        }

        public async Task<IEnumerable<HtmlDocument>> GetMatchingDetailsPagesAsync(string lastName, string firstName, TitleEnum? title)
        {
            var document = await SkosAccessor.GetPageForSearchResult(lastName, firstName, title);

            var titleNode = document.DocumentNode.SelectSingleNode("/html/head/title");

            if (titleNode.InnerText.Contains("Wyniki wyszukiwania"))
            {
                if (document.DocumentNode.InnerHtml.Contains("Nie znaleziono os&oacute;b pasuj&#261;cych do zadanych kryteri&oacute;w."))
                {
                    return new HtmlDocument[] { };
                }

                var peopleTable = document.QuerySelector(".lista-osob");
                var peopleLinks = peopleTable.SelectNodes(".//th").Nodes();

                var detailDocuments = peopleLinks.Select(async item =>
                {
                     return await SkosAccessor.GetPageForUrl(item.Attributes["href"].Value);
                });
                return await Task.WhenAll(detailDocuments);
            }
            else
            {
                return new HtmlDocument[] { document };
            }
        }
    }
}
