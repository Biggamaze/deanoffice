﻿using SKOS.Scraper.Entities.HtmlNodes;
using SKOS.Scraper.Utility;
using System.Linq;
using System.Net;

namespace SKOS.Scraper.NodeParsers
{
    internal static class NameNodeParser
    {
        public static string GetFirstName(this NameNode node)
        {
            return node.Descendants
                .Where(x => x.HasClass("given-name"))
                .Select(x => WebUtility.HtmlDecode(x.InnerHtml))
                .First()
                .RemoveSpecialCharacters();
        }

        public static string GetLastName(this NameNode node)
        {
            return node.Descendants
                .Where(x => x.HasClass("family-name"))
                .Select(x => WebUtility.HtmlDecode(x.InnerHtml))
                .First()
                .RemoveSpecialCharacters();
        }
    }
}
