﻿using SKOS.Scraper.Entities.HtmlNodes;
using System;
using System.Text.RegularExpressions;

namespace SKOS.Scraper.NodeSelectors
{
    internal static class EmailNodeParser
    {
        public static string GetEmail(this EmailNode node)
        {
            var encrypted = GetEncryptedMailString(node);
            var email = GetMailFromEncryptedString(encrypted);

            return email;
        }

        internal static string GetEncryptedMailString(EmailNode node)
        {
            var encString = node.Attributes["data-html"].Value;

            return encString;
        }
        
        internal static string GetMailFromEncryptedString(string encrypted)
        {
            var chArray = encrypted.ToCharArray();
            Array.Reverse(chArray);
            var inverted = new String(chArray);

            var replaced = inverted.Replace("#", "@");
            var regex = new Regex("(?<=tg&).*?(?=;tl&)");
            var match = regex.Match(replaced).Value;

            return match;
        }
    }
}
