﻿using SKOS.Scraper.Entities.Result;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using SKOS.Scraper.NodeSelectors;
using SKOS.Scraper.NodeParsers;
using DeanOffice.Common.Enums;

namespace SKOS.Scraper
{
    public class SkosChecker
    {
        private HtmlGetter SkosAccessor { get; }

        public SkosChecker()
        {
            SkosAccessor = new HtmlGetter();
        }

        public async Task<IEnumerable<SkosResult>> GetSkosMatches(string firstName, string lastName, TitleEnum? title)
        {
            var matchingPages = await SkosAccessor.GetMatchingDetailsPagesAsync(firstName, lastName, title);

            return matchingPages.Select(x => new
            {
                FullName = x.GetFullNameNode(),
                Email = x.GetEmailNode()
            })
            .Select(x => new SkosResult
            {
                FirstName = x.FullName.GetFirstName(),
                LastName = x.FullName.GetLastName(),
                Email = x.Email.GetEmail()
            });
        }

        public async Task<IEnumerable<SkosResult>> GetSkosMatches(string firstName, string lastName)
        {
            return await GetSkosMatches(firstName, lastName, null);
        }

        public async Task<IEnumerable<SkosResult>> GetSkosMatches(string lastName, TitleEnum title)
        {
            return await GetSkosMatches(lastName, string.Empty, title);
        }

        public async Task<IEnumerable<SkosResult>> GetSkosMatches(string lastName)
        {
            return await GetSkosMatches(string.Empty, lastName, null);
        }
    }
}
