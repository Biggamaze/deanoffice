﻿using DeanOffice.Common.Enums;
using HtmlAgilityPack;
using System.Runtime.CompilerServices;
using System.Threading.Tasks;

[assembly: InternalsVisibleTo("SKOS.Scraper.Tests")]
namespace SKOS.Scraper
{
    internal class HtmlGetterSkosAccessor
    {
        private HtmlWeb WebAccessor { get; }

        private const string UrlBase = "https://skos.agh.edu.pl/search/?nazwisko={0}&imie={1}&tytul={2}";

        public HtmlGetterSkosAccessor()
        {
            WebAccessor = new HtmlWeb();
        }

        public async Task<HtmlDocument> GetPageForUrl(string url)
        {
            return await WebAccessor.LoadFromWebAsync(url);
        }

        public async Task<HtmlDocument> GetPageForSearchResult(string lastName, string firstName, TitleEnum? title)
        {
            var url = GeneratUrl(lastName, firstName, title);
            return await WebAccessor.LoadFromWebAsync(url);
        }

        internal string GeneratUrl(string lastName, string firstName = "", TitleEnum? title = null)
        {
            string url;
            if (title == null)
            {
                url = string.Format(UrlBase, lastName, firstName, string.Empty);
            }
            else
            {
                url = string.Format(UrlBase, lastName, firstName, (int)title);
            }

            return url;
        }
    }
}
