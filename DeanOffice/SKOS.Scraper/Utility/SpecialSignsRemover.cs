﻿using System.Linq;

namespace SKOS.Scraper.Utility
{
    internal static class SpecialSignsRemover
    {
        public static string RemoveSpecialCharacters(this string str)
        {
            return new string(str.Where(x => char.IsLetterOrDigit(x)).ToArray());
        }
    }
}
