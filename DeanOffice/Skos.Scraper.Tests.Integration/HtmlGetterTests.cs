using DeanOffice.Common.Enums;
using SKOS.Scraper;
using Xunit;

namespace Skos.Scraper.Tests.Integration
{
    public class HtmlGetterTests
    {
        [Theory]
        [InlineData("Zol", "", null)]
        [InlineData("Zoladek", "Maciej", TitleEnum.DrInz)]
        [InlineData("", "Andrzej", TitleEnum.DrHab)]
        [InlineData("", "", null)]
        public void ShouldNotThrowException_When_Searching(string lastName, string firstName, TitleEnum? title)
        {
            var getter = new HtmlGetter();

            var matching = getter.GetMatchingDetailsPagesAsync(lastName, firstName, title).Result;
        }
    }
}
