﻿using SKOS.Scraper;
using System.Linq;
using Xunit;

namespace Skos.Scraper.Tests.Integration
{
    public class SkosCheckerTests
    {
        [Fact]
        public void GetSkosMatches_ValidDataInput_DoesNotThrowException()
        {
            var skosChecker = new SkosChecker();

            var results = skosChecker.GetSkosMatches("Zoladek", "Maciej").Result;
        }

        [Fact]
        public void GetSkosMatches_ValidInputData_ListIsNotEmpty()
        {
            var skosChecker = new SkosChecker();

            var results = skosChecker.GetSkosMatches("Zoladek", "Maciej").Result;

            Assert.NotEmpty(results);
        }

        [Fact]
        public void GetSkosMatches_ValidInputData_ResultsContainExpectedData()
        {
            var skosChecker = new SkosChecker();

            var results = skosChecker.GetSkosMatches("Zoladek", "Maciej").Result;

            var contains = results.Any(x =>
            {
                return (x.FirstName == "Maciej") &&
                (x.LastName == "Żołądek") &&
                (x.Email == "mzoladek@agh.edu.pl");
            });

            Assert.True(contains);
        }

    }
}
