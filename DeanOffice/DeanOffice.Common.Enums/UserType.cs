﻿namespace DeanOffice.Common.Enums
{
    public enum UserType
    {
        Student = 0,
        Professor = 1
    }
}
