﻿namespace DeanOffice.Common.Enums
{
    public enum TitleEnum
    {
        Brak = 1,
        Dr,
        DrHab,
        DrHabInz,
        DrInz,
        DrInzArchitekt,
        DyplEk,
        Inz,
        Mgr,
        MgrInz,
        MgrInzArch,
        ProfDrHab,
        ProfDrHabInz,
        ProfNadzwDrHab = 15,
        ProfNadzwDrHabInz,
        ProfZwDrHab,
        ProfZwDrHabInz,
        ProfZwDrInz,
        Licencjat = 24
    }
}
