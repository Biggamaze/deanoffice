﻿using DeanOffice.Identity.Interfaces;
using SKOS.Scraper;
using System.Linq;
using System.Threading.Tasks;

namespace DeanOffice.Identity.RegistrationUtils
{
    public class ProfessorValidityChecker : IUserValidator
    {
        public async Task<bool> Check(ApplicationUser user)
        {
            var checker = new SkosChecker();

            var matches = await checker.GetSkosMatches(user.LastName, user.FirstName);

            return matches.Any(x => x.FirstName.ToLower() == user.FirstName.ToLower() &&
                                    x.LastName.ToLower() == user.LastName.ToLower() &&
                                    x.Email.ToLower() == user.Email.ToLower());
        }
    }
}
