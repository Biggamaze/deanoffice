﻿using System.Threading.Tasks;

namespace DeanOffice.Identity.Interfaces
{
    public interface IUserValidator
    {
        Task<bool> Check(ApplicationUser user);
    }
}
