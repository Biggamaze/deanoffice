﻿using DeanOffice.Common.Enums;
using Microsoft.AspNetCore.Identity;
using System;

namespace DeanOffice.Identity
{
    public class ApplicationUser : IdentityUser<Guid>
    {
        public ApplicationUser()
        {

        }

        public ApplicationUser(string userName)
            :base(userName)
        {

        }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public TitleEnum Title { get; set; }
    }
}
