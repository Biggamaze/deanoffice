﻿using DeanOffice.Identity.Models;
using Microsoft.AspNetCore.Identity;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Security.Claims;

namespace DeanOffice.Identity
{
    public class InitializeIdentity
    {
        private readonly IServiceProvider _serviceProvider;

        public InitializeIdentity(IServiceProvider serviceProvider)
        {
            _serviceProvider = serviceProvider;
        }

        public void InitializeRootUser()
        {

            var logger = _serviceProvider.GetRequiredService<ILogger<InitializeIdentity>>();
            var userManager = _serviceProvider.GetRequiredService<UserManager<ApplicationUser>>();
            {
                var rootUser = userManager.FindByNameAsync("root").Result;
                if (rootUser == null)
                {
                    rootUser = new ApplicationUser("root")
                    {
                        Id = Guid.Parse("00000000-0000-0000-0000-000000000001")
                    };

                    var result = userManager.CreateAsync(rootUser, "ichuj").Result;
                    var claims = new[]
                    {
                     new Claim(DeanOfficeClaims.Role, DeanOfficeRoles.Root),
                     new Claim(DeanOfficeClaims.Navigation, "00000000-0000-0000-0000-000000000001")
                    };
                    result = userManager.AddClaimsAsync(rootUser, claims).Result;

                    if (result.Succeeded)
                    {
                        logger.LogInformation("Succesfully created root user");
                    }
                    else
                    {
                        logger.LogError("Failed creating root user");
                        foreach (var error in result.Errors)
                        {
                            logger.LogError(error.Description);
                        }
                    }
                }
            }
        }

        public IEnumerable<Guid> InitializeStudentUsers()
        {
            var guids = new[]
            {
                Guid.Parse("c2897dea-2368-4f69-ae12-0ee7dc5e0ee0"),
                Guid.Parse("e0f566fd-577e-4f5f-aa25-3c7eb00860cb"),
                Guid.Parse("8dc62336-da0e-4695-b2ec-9d4d561477ec"),
            };
            var names = new[] { "Janek", "Ania", "Natlia" };

            InitializeUsersImpl(guids, names, DeanOfficeRoles.Student);

            return guids;
        }

        public IEnumerable<Guid> InitializeProfessorUsers()
        {
            var guids = new[]
            {
                Guid.Parse("00a44b2c-191a-41eb-b659-361b9cc10848"),
                Guid.Parse("ea29844f-0d60-4ae6-bf70-c9d9d6883c16"),
                Guid.Parse("d481df8c-c8db-49f8-9028-a25a9884821c"),
            };
            var names = new[] { "Jan", "Anna", "Cezary" };

            InitializeUsersImpl(guids, names, DeanOfficeRoles.Professor);

            return guids;
        }

        private void InitializeUsersImpl(Guid[] guids, string[] userNames, string role)
        {
            if (Environment.GetEnvironmentVariable("ASPNETCORE_ENVIRONMENT") != "Development")
            {
                return;
            }

            var students = new[]
            {
                new
                {
                    User = new ApplicationUser(userNames[0])
                    {
                        Id = guids[0]
                    },
                    Claims = new[]
                    {
                         new Claim(DeanOfficeClaims.Role, role),
                         new Claim(DeanOfficeClaims.Navigation, guids[0].ToString())
                    }
                },
                new
                {
                    User = new ApplicationUser(userNames[1])
                    {
                        Id = guids[1]
                    },
                    Claims = new[]
                    {
                         new Claim(DeanOfficeClaims.Role, role),
                         new Claim(DeanOfficeClaims.Navigation, guids[1].ToString())
                    }
                },
                new
                {
                    User = new ApplicationUser(userNames[2])
                    {
                        Id = guids[2]
                    },
                    Claims = new[]
                    {
                         new Claim(DeanOfficeClaims.Role, role),
                         new Claim(DeanOfficeClaims.Navigation, guids[2].ToString())
                    }
                },
            };

            var logger = _serviceProvider.GetRequiredService<ILogger<InitializeIdentity>>();
            var userManager = _serviceProvider.GetRequiredService<UserManager<ApplicationUser>>();
            {
                foreach (var entity in students)
                {
                    if (userManager.FindByIdAsync(entity.User.Id.ToString()).Result != null)
                    {
                        continue;
                    }

                    var isSuccessfullySeeded = userManager.CreateAsync(entity.User, entity.User.UserName.ToLower()).Result.Succeeded
                                    && userManager.AddClaimsAsync(entity.User, entity.Claims).Result.Succeeded;

                    if (isSuccessfullySeeded)
                    {
                        logger.LogInformation($"Succesfully seeded user {entity.User.UserName}");
                    }
                    else
                    {
                        throw new ApplicationException($"Error seeding user {entity.User.UserName}");
                    }
                }
            }
        }
    }
}
