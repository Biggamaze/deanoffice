﻿namespace DeanOffice.Identity.Models
{
    public static class DeanOfficeClaims
    {
        public static readonly string Navigation = "Navigation";
        public static readonly string Role = "Role";
    }
}
