﻿namespace DeanOffice.Identity.Models
{
    public static class DeanOfficeRoles
    {
        public const string Professor = "professor";
        public const string Student = "student";
        public const string DeanOfficer = "deanOfficer";
        public const string Root = "root";
    }
}
