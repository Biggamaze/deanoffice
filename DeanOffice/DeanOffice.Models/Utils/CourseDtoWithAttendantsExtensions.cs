﻿using DeanOffice.Models.DataTransferObjects.Course;
using System;
using System.Linq;

namespace DeanOffice.Models.Utils
{
    public static class CourseDtoWithAttendantsExtensions
    {
        public static bool IsPersonSignedForCourse(this CourseDtoWithAttendants course, Guid id)
        {
            return course.Attendants.Any(a => a.Id == id) || course.Lecturer.Id == id;
        } 
    }
}
