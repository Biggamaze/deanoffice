﻿using DeanOffice.Models.DataTransferObjects.Student;
using System.Collections.Generic;
using System.Linq;

namespace DeanOffice.Models.DataTransferObjects.Course
{
    public class CourseDtoWithAttendants : CourseDto
    {
        public IEnumerable<StudentBasicDto> Attendants { get; set; } = Enumerable.Empty<StudentBasicDto>();
    }
}
