﻿using DeanOffice.Models.DataTransferObjects.Professor;
using DeanOffice.Models.DataTransferObjects.Subject;
using System;

namespace DeanOffice.Models.DataTransferObjects.Course
{
    public class CourseDto
    {
        public Guid CourseId { get; set; }
        public string Name { get; set; }
        public ProfessorBasicDto Lecturer { get; set; }
        public int SubjectId { get; set; }
        public DayOfWeek DayOfWeek { get; set; }
        public SubjectBasicDto Subject { get; set; }
        public DateTime StartupDate { get; set; }
        public DateTime FinishDate { get; set; }
        public TimeSpan StartHour { get; set; }
        public TimeSpan EndHour { get; set; }
        public CourseTypeDto Type { get; set; }
        public int StudentCountLimit { get; set; }
        public int CurrentStudentCount { get; set; }
    }
}
