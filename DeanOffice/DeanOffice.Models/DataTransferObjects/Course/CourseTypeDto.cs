﻿namespace DeanOffice.Models.DataTransferObjects.Course
{
    public class CourseTypeDto
    {
        public int TypeId { get; set; }
        public string Name { get; set; }
    }
}
