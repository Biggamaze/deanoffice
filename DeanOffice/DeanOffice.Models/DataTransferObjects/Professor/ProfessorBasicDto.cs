﻿using System;

namespace DeanOffice.Models.DataTransferObjects.Professor
{
    public class ProfessorBasicDto
    {
        public Guid Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string FullName => $"{FirstName} {LastName}";
    }
}
