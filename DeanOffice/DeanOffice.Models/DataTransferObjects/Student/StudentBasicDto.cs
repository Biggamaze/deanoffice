﻿using System;

namespace DeanOffice.Models.DataTransferObjects.Student
{
    public class StudentBasicDto
    {
        public Guid Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
    }
}
