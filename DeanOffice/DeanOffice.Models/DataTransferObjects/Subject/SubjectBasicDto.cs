﻿using System;

namespace DeanOffice.Models.DataTransferObjects.Subject
{
    public class SubjectBasicDto
    {
        public Guid SubjectId { get; set; }
        public string Name { get; set; }
    }
}
