﻿namespace DeanOffice.Models.ViewModels
{
    public abstract class ViewModelBase
    {
        public string ReturnUrl { get; set; }
        public bool HasAdminRights { get; set; } = false;
        public bool IsStudent { get; set; } = false;
        public bool IsLecturer { get; set; } = false;
    }
}
