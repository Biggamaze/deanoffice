﻿using DeanOffice.Common.Enums;
using System.ComponentModel.DataAnnotations;

namespace DeanOffice.Models.ViewModels.Account
{
    public class RegisterViewModel : ViewModelBase
    {
        [Required]
        [EnumDataType(typeof(UserType))]
        public UserType UserType { get; set; }

        [Required]
        [Display(Name = "First name")]
        public string FirstName { get; set; }

        [Required]
        [Display(Name = "Last name")]
        public string LastName { get; set; }

        [Required]
        [DataType(DataType.EmailAddress)]
        public string Email { get; set; }

        [EnumDataType(typeof(TitleEnum))]
        public TitleEnum Title { get; set; }

        [Required]
        public string Username { get; set; }

        [Required]
        [DataType(DataType.Password)]
        [StringLength(50, MinimumLength = 6, ErrorMessage = "Password must be between 6 and 50 characters")]
        public string Password { get; set; }

        [Required]
        [DataType(DataType.Password)]
        [Compare(nameof(Password), ErrorMessage = "Password and password confirmation do not match")]
        public string ConfirmPassword { get; set; }
    }
}
