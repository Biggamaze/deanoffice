﻿using System.ComponentModel.DataAnnotations;

namespace DeanOffice.Models.ViewModels.Account
{
    public class LoginViewModel : ViewModelBase
    {
        [Required]
        public string Username { get; set; }

        [Required]
        [DataType(DataType.Password)]
        public string Password { get; set; }

        [Required]
        [Display(Name = "Remember me?")]
        public bool RememberMe { get; set; }
    }
}
