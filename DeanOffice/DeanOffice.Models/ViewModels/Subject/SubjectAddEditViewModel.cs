﻿namespace DeanOffice.Models.ViewModels.Subject
{
    public class SubjectAddEditViewModel : ViewModelBase
    {
        public Data.DatabaseEntities.Subject Subject { get; set; }
    }
}
