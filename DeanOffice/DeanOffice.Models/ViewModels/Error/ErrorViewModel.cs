﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DeanOffice.Models.ViewModels.Error
{
    public class ErrorViewModel : ViewModelBase
    {
        public string ErrorMessage { get; set; }
    }
}
