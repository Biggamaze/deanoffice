﻿using DeanOffice.Models.DataTransferObjects.Course;
using DeanOffice.Models.DataTransferObjects.Professor;
using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc.Rendering;
using System.Linq;
using DeanOffice.Models.DataTransferObjects.Subject;

namespace DeanOffice.Models.ViewModels.Course
{
    public class CourseEditViewModel : ViewModelBase
    {
        private IEnumerable<SubjectBasicDto> _subjects;
        private IEnumerable<ProfessorBasicDto> _availableLecturers;
        private IEnumerable<CourseTypeDto> _courseTypes;

        public CourseDto Course { get; set; }

        public IEnumerable<SelectListItem> GetSubjects()
        {
            return _subjects.Select(sub => new SelectListItem { Text = sub.Name, Value = sub.SubjectId.ToString() });
        }
        public void SetSubjects(IEnumerable<SubjectBasicDto> subjects)
        {
            _subjects = subjects;
        }

        public IEnumerable<SelectListItem> GetLecturers()
        {
            return _availableLecturers.Select(lec => new SelectListItem { Text = lec.FullName, Value = lec.Id.ToString() });
        }
        public void SetLecturers(IEnumerable<ProfessorBasicDto> lecturers)
        {
            _availableLecturers = lecturers;
        }

        public IEnumerable<SelectListItem> GetTypes()
        {
            return _courseTypes.Select(lec => new SelectListItem { Text = lec.Name, Value = lec.TypeId.ToString() });
        }
        public void SetTypes(IEnumerable<CourseTypeDto> types)
        {
            _courseTypes = types;
        }
    }
}
