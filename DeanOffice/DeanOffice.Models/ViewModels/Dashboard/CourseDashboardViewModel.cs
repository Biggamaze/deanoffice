﻿using DeanOffice.Models.DataTransferObjects.Course;
using System.Collections.Generic;

namespace DeanOffice.Models.ViewModels.Dashboard
{
    public class CourseDashboardViewModel : ViewModelBase
    {
        public string UserRole { get; set; }
        public string Name { get; set; }
        public string Surname { get; set; }
        public IEnumerable<CourseDto> Courses { get; set; }
    }
}
