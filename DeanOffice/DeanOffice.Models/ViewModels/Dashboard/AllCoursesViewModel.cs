﻿using System;
using System.Collections.Generic;
using System.Text;
using DeanOffice.Models.DataTransferObjects.Course;

namespace DeanOffice.Models.ViewModels.Dashboard
{
    public class AllCoursesViewModel : ViewModelBase
    {
        public IEnumerable<CourseDto> Courses { get; set; }
    }
}
