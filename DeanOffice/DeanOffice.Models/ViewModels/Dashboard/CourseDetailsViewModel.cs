﻿using DeanOffice.Models.DataTransferObjects.Course;

namespace DeanOffice.Models.ViewModels.Dashboard
{
    public class CourseDetailsViewModel : ViewModelBase
    {
        public CourseDtoWithAttendants Course { get; set; }
        public bool IsPersonSignedUp { get; set; }
    }
}
