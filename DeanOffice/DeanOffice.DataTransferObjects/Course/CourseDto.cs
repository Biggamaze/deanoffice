﻿using DeanOffice.Data.DatabaseEntities;
using DeanOffice.DataTransferObjects.Professor;
using System;

namespace DeanOffice.DataTransferObjects.Course
{
    public class CourseDto
    {
        public int CourseId { get; set; }
        public ProfessorBasicDto Lecturer { get; set; }
        public string Name { get; set; }
        public int SubjectId { get; set; }
        public Subject Subject { get; set; }
        public DateTime StartTime { get; set; }
        public DateTime EndTime { get; set; }
        public TimeSpan Duration => EndTime - StartTime;
    }
}
