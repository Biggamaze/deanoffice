﻿using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc.ModelBinding;

namespace DeanOffice.Application.Common
{
    internal static class ControllerUtilities
    {
        public static void AddErrors(this ModelStateDictionary model, IdentityResult result)
        {
            foreach (var error in result.Errors)
            {
                model.AddModelError(string.Empty, error.Description);
            }
        }
    }
}
