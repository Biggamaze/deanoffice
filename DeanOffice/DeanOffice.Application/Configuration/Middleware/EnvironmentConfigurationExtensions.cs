﻿using Microsoft.AspNetCore.Builder;
using System;

namespace DeanOffice.Application.Configuration.Middleware
{
    internal static class EnvironmentConfigurationExtensions
    {
        public static void ConfigureGlobalFlags(this IApplicationBuilder app)
        {
#if DEBUG
            Environment.SetEnvironmentVariable("ASPNETCORE_ENVIRONMENT", "Development");
#else
            Environment.SetEnvironmentVariable("ASPNETCORE_ENVIRONMENT", "Production");
#endif
        }
    }
}
