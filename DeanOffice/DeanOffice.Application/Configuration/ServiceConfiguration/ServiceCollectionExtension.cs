﻿using DeanOffice.Data;
using DeanOffice.Identity;
using DeanOffice.Identity.Interfaces;
using DeanOffice.Identity.Models;
using DeanOffice.Identity.RegistrationUtils;
using DeanOffice.Repository;
using DeanOffice.Repository.Interfaces;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using System;

namespace DeanOffice.Application.Configuration.ServiceConfiguration
{
    internal static class ServiceCollectionExtensions
    {
        public static void AddDatabaseContext(this IServiceCollection services, IConfiguration configuration)
        {
            services.AddDbContext<DeanOfficeEntities>(options =>
            {
                options.UseSqlServer(configuration.GetConnectionString("DeanOfficeDb"));
            });
        }

        public static void AddIdentity(this IServiceCollection services, IConfiguration configuration)
        {
            services.AddDbContext<ApplicationIdentityContext>(options =>
            {
                options.UseSqlServer(configuration.GetConnectionString("DeanOfficeIdentity"));
            });

            services.AddIdentity<ApplicationUser, ApplicationRole>()
                    .AddEntityFrameworkStores<ApplicationIdentityContext>()
                    .AddDefaultTokenProviders()
                    .AddSignInManager<DeanOfficeSignInManager<ApplicationUser>>();

            services.AddScoped<SignInManager<ApplicationUser>, DeanOfficeSignInManager<ApplicationUser>>();
        }

        public static void AddRepositories(this IServiceCollection services)
        {
            services.AddScoped<IStudentRepository, StudentRepository>();
            services.AddScoped<IProfessorRepository, ProfessorRepository>();
            services.AddScoped<ICourseRepository, CourseRepository>();
            services.AddScoped<ISubjectRepository, SubjectRepository>();
            services.AddScoped<ICurriculumRepository, CurriculumRepository>();
        }

        public static void ConfigurePasswordPolicy(this IServiceCollection services)
        {
            if (Environment.GetEnvironmentVariable("ASPNETCORE_ENVIRONMENT") == "Development")
            {
                services.Configure<IdentityOptions>(options =>
                {
                    options.Password.RequireDigit = false;
                    options.Password.RequiredUniqueChars = 0;
                    options.Password.RequireNonAlphanumeric = false;
                    options.Password.RequiredLength = 1;
                    options.Password.RequireUppercase = false;
                    options.Password.RequiredLength = 0;
                });
            }
        }

        public static void AddUserValidators(this IServiceCollection services)
        {
            services.AddScoped<IUserValidator, ProfessorValidityChecker>();
        }

        public static void ConfigureAuthorizationPolicies(this IServiceCollection services)
        {
            services.AddAuthorization(options =>
            {
                options.AddPolicy("IsValidUser", policy =>
                    policy.RequireAssertion(context =>
                        context.User.HasClaim(claim => claim.Type == DeanOfficeClaims.Navigation)
                        && context.User.HasClaim(claim => claim.Type == DeanOfficeClaims.Role)));

                options.AddPolicy("DeanOfficer", policy =>
                    policy.RequireClaim(DeanOfficeClaims.Role, DeanOfficeRoles.DeanOfficer, DeanOfficeRoles.Root));
            });
        }
    }
}
