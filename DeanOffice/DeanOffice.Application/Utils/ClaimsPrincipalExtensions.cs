﻿using DeanOffice.Identity.Models;
using System;
using System.Linq;
using System.Security.Claims;

namespace DeanOffice.Application.Utils
{
    public static class ClaimsPrincipalExtensions
    {
        public static Guid GetGuid(this ClaimsPrincipal claimsPrincipal)
        {
            return Guid.Parse(((ClaimsIdentity)claimsPrincipal.Identity).Claims
                .First(cl => cl.Type == DeanOfficeClaims.Navigation).Value);
        }

        public static string GetRole(this ClaimsPrincipal claimsPrincipal)
        {
            return ((ClaimsIdentity)claimsPrincipal.Identity).Claims
                .First(cl => cl.Type == DeanOfficeClaims.Role).Value;
        }

        public static bool IsStudent(this ClaimsPrincipal claimsPrincipal)
        {
            return claimsPrincipal.GetRole() == DeanOfficeRoles.Student;
        }

        public static bool HasElevatedRights(this ClaimsPrincipal claimsPrincipal)
        {
            return elevatedRoles.Any(r => claimsPrincipal.GetRole() == r);
        }

        public static bool HasAdminRights(this ClaimsPrincipal claimsPrincipal)
        {
            return adminRoles.Any(r => claimsPrincipal.GetRole() == r);
        }

        private static readonly string[] elevatedRoles = new[]
        {
            DeanOfficeRoles.Root,
            DeanOfficeRoles.DeanOfficer,
            DeanOfficeRoles.Root
        };

        private static readonly string[] adminRoles = new[]
        {
            DeanOfficeRoles.Root,
            DeanOfficeRoles.DeanOfficer,
        };
    }
}
