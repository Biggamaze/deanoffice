﻿using DeanOffice.Common.Enums;
using DeanOffice.Identity;
using DeanOffice.Identity.Models;
using DeanOffice.Models.ViewModels.Account;
using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.Security.Claims;
using System.Threading.Tasks;

namespace DeanOffice.Application.Utils
{
    public static class UserManagerExtensions
    {
        internal static async Task<IdentityResult> AddClaimsAsyncTo(this UserManager<ApplicationUser> userManager,
            ApplicationUser user, RegisterViewModel registerModel)
        {
            var result = await AddCommonClaims(userManager, user);
            result = await userTypeClaimAdders[registerModel.UserType](userManager, user);

            return result;
        }

        private static async Task<IdentityResult> AddCommonClaims(UserManager<ApplicationUser> userManager, ApplicationUser user)
        {
            var commonClaims = new[]
            {
                new Claim(DeanOfficeClaims.Navigation, user.Id.ToString())
            };
            return await userManager.AddClaimsAsync(user, commonClaims);
        }

        private static async Task<IdentityResult> AddProfessorClaims(UserManager<ApplicationUser> userManager, ApplicationUser user)
        {
            var professorClaims = new[]
            {
                new Claim(DeanOfficeClaims.Role, DeanOfficeRoles.Professor)
            };
            return await userManager.AddClaimsAsync(user, professorClaims);
        }

        private static async Task<IdentityResult> AddStudentClaims(UserManager<ApplicationUser> userManager, ApplicationUser user)
        {
            var studentClaims = new[]
            {
                new Claim(DeanOfficeClaims.Role, DeanOfficeRoles.Student)
            };
            return await userManager.AddClaimsAsync(user, studentClaims);
        }

        private static IDictionary<UserType, Func<UserManager<ApplicationUser>, ApplicationUser, Task<IdentityResult>>> userTypeClaimAdders =
            new Dictionary<UserType, Func<UserManager<ApplicationUser>, ApplicationUser, Task<IdentityResult>>>
            {
                { UserType.Professor, AddProfessorClaims },
                { UserType.Student,  AddStudentClaims }
            };
    }
}
