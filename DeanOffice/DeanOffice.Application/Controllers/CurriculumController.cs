﻿using System;
using DeanOffice.Models.ViewModels.Error;
using DeanOffice.Repository.Interfaces;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace DeanOffice.Application.Controllers
{
    [Route("curriculum")]
    [Authorize(Policy = "IsValidUser")]
    public class CurriculumController : Controller
    {
        private readonly ICurriculumRepository _curriculum;
        private readonly ICourseRepository _course;

        public CurriculumController(ICurriculumRepository curriculum, ICourseRepository courseRepo)
        {
            _curriculum = curriculum;
            _course = courseRepo;
        }

        [HttpPost("sigup/{courseId:guid}/{studentId:guid}")]
        public IActionResult SignUpForACourse(Guid courseId, Guid studentId, [FromQuery]string returnUrl)
        {
            var course = _course.GetCourse(courseId);
            if (course.Students.Count >= course.StudentCountLimit)
            {
                var errorModel = new ErrorViewModel
                {
                    ErrorMessage =
                        "Maximum number of students being signed up has been reached. You can't be signed up",
                    ReturnUrl = returnUrl
                };
                return View("Error", errorModel);
            }
            _curriculum.SignUpForLecture(studentId, courseId);

            return RedirectToAction("MainPanel", "Dashboard");
        }

        [HttpPost("sigout/{courseId:guid}/{studentId:guid}")]
        public IActionResult SignOutFromACourse(Guid courseId, Guid studentId)
        {
            _curriculum.SignOutFromLecture(studentId, courseId);

            return RedirectToAction("MainPanel", "Dashboard");
        }
    }
}