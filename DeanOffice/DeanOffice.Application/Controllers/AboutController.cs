﻿using Microsoft.AspNetCore.Mvc;

namespace DeanOffice.Application.Controllers
{
    [Route("about")]
    public class AboutController : Controller
    {
        [HttpGet("contact")]
        public IActionResult Contact()
        {
            return View();
        }
    }
}