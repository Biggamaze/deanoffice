﻿using Microsoft.AspNetCore.Mvc;
using DeanOffice.Models.ViewModels.Error;

namespace DeanOffice.Application.Controllers
{
    [Route("")]
    public class HomeController : Controller
    {
        [HttpGet("")]
        public IActionResult Index()
        {
            return View();
        }

        [HttpGet("error")]
        public IActionResult Error(ErrorViewModel model)
        {
            return View(model);
        }
    }
}
