﻿using System;
using DeanOffice.Data.DatabaseEntities;
using DeanOffice.Models.ViewModels.Subject;
using DeanOffice.Repository.Interfaces;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace DeanOffice.Application.Controllers
{
    [Authorize(Policy = "DeanOfficer")]
    [Route("subjects")]
    public class SubjectController : Controller
    {
        private readonly ISubjectRepository _subjectRepository;

        public SubjectController(ISubjectRepository subjectRepository)
        {
            _subjectRepository = subjectRepository;
        }

        [HttpGet("")]
        public IActionResult GetAllSubjects()
        {
            var subjects = _subjectRepository.GetSubjects();

            return View(subjects);
        }

        [HttpGet("add")]
        public IActionResult AddSubject([FromQuery]string returnUrl)
        {
            var model = new SubjectAddEditViewModel()
            {
                ReturnUrl = returnUrl,
                Subject = new Subject()
            };

            return View("AddEditSubject", model);
        }

        [HttpGet("edit")]
        public IActionResult EditSubject([FromQuery]string returnUrl, [FromQuery] string subjectId)
        {
            var model = new SubjectAddEditViewModel()
            {
                ReturnUrl = returnUrl,
                Subject = _subjectRepository.GetSubject(Guid.Parse(subjectId))
            };

            return View("AddEditSubject", model);
        }

        [HttpPost("add")]
        public IActionResult AddSubject(SubjectAddEditViewModel model)
        {
            if (!ModelState.IsValid)
            {
                return View("AddEditSubject", model);
            }

            _subjectRepository.AddSubject(model.Subject);
            return Redirect(model.ReturnUrl);
        }

        [HttpPost("edit")]
        public IActionResult EditSubject(SubjectAddEditViewModel model)
        {
            if (!ModelState.IsValid)
            {
                return View("AddEditSubject", model);
            }

            _subjectRepository.UpdateSubject(model.Subject);
            return Redirect(model.ReturnUrl);
        }
    }
}