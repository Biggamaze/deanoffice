﻿using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Authorization;
using Microsoft.Extensions.Logging;
using DeanOffice.Identity;
using DeanOffice.Application.Common;
using DeanOffice.Identity.Interfaces;
using DeanOffice.Common.Enums;
using DeanOffice.Application.Utils;
using System;
using DeanOffice.Models.ViewModels.Account;

namespace DeanOffice.Application.Controllers
{
    [Route("account")]
    public class AccountController : Controller
    {
        private readonly UserManager<ApplicationUser> _userManager;
        private readonly SignInManager<ApplicationUser> _signInManager;
        private readonly ILogger<AccountController> _logger;
        private readonly IUserValidator _userValidator;

        public AccountController(UserManager<ApplicationUser> userManager,
                                 SignInManager<ApplicationUser> signInManager,
                                 ILogger<AccountController> logger,
                                 IUserValidator userValidator)
        {
            _userManager = userManager;
            _signInManager = signInManager;
            _logger = logger;
            _userValidator = userValidator;
        }

        [HttpGet("register")]
        [AllowAnonymous]
        public IActionResult Register()
        {
            return View();
        }

        [HttpPost("register")]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Register(RegisterViewModel model)
        {
            if (ModelState.IsValid)
            {
                var user = new ApplicationUser
                {
                    Id = Guid.NewGuid(),
                    UserName = model.Username,
                    FirstName = model.FirstName,
                    LastName = model.LastName,
                    Email = model.Email,
                    Title = model.Title
                };

                if (model.UserType == UserType.Professor && !await _userValidator.Check(user))
                {
                    ModelState.AddModelError(string.Empty, "Such professor does not exist");
                    return View(model);
                }

                var result = await _userManager.CreateAsync(user, model.Password);
                if (result.Succeeded)
                {
                    _logger.LogInformation($"Succesuflly created user {user.UserName}");

                    _logger.LogTrace($"Adding claims to user {user.UserName}");
                    await _userManager.AddClaimsAsyncTo(user, model);
                    _logger.LogTrace($"Finished adding claims");

                    _logger.LogTrace($"Starting logging user {user.UserName}");
                    await _signInManager.SignInAsync(user, isPersistent: false);
                    _logger.LogInformation($"User {user.UserName} is now signed in");
                    return RedirectToAction(nameof(HomeController.Index), "Home");
                }
                ModelState.AddErrors(result);
            }
            return View(model);
        }

        [HttpGet("login")]
        [AllowAnonymous]
        public IActionResult Login()
        {
            return View();
        }

        [HttpPost("login")]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Login(LoginViewModel model)
        {
            if (ModelState.IsValid)
            {
                var loginResult = await _signInManager.PasswordSignInAsync(
                    model.Username, model.Password, isPersistent: model.RememberMe, lockoutOnFailure: false);
                if (loginResult.Succeeded)
                {
                    _logger.LogInformation($"Succesfully signed {model.Username} in");
                    return RedirectToAction(nameof(HomeController.Index), "Home");
                }
                if (loginResult.IsLockedOut)
                {
                    _logger.LogInformation($"{model.Username} account locked out");
                    return RedirectToAction(nameof(HomeController.Index), "Home");
                }

                _logger.LogInformation($"Unsuccesful attempt to login by {model.Username}");
                ModelState.AddModelError(string.Empty, "Login attempt failed");
                return View(model);
            }
            return View(model);
        }

        [HttpPost("logout")]
        [Authorize(Policy = "IsValidUser")]
        public async Task<IActionResult> Logout()
        {
            var userName = User.Identity.Name;
            await _signInManager.SignOutAsync();
            _logger.LogInformation($"Succesfully signed {userName} out");
            return RedirectToAction(nameof(HomeController.Index), "Home");
        }
    }
}