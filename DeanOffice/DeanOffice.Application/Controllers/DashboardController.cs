﻿using System;
using System.Collections.Generic;
using AutoMapper;
using DeanOffice.Application.Utils;
using DeanOffice.Identity.Models;
using DeanOffice.Models.DataTransferObjects.Course;
using DeanOffice.Models.Utils;
using DeanOffice.Models.ViewModels.Dashboard;
using DeanOffice.Repository.Interfaces;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace DeanOffice.Application.Controllers
{
    [Authorize(Policy = "IsValidUser")]
    [Route("dashboard")]
    public class DashboardController : Controller
    {
        private readonly ICourseRepository _courses;
        private readonly IMapper _mapper;
        private readonly IStudentRepository _studentRepository;
        private readonly IProfessorRepository _professorRepository;

        public DashboardController(ICourseRepository courses, IMapper mapper,
            IStudentRepository studentRepo, IProfessorRepository profRepo)
        {
            _courses = courses;
            _mapper = mapper;
            _studentRepository = studentRepo;
            _professorRepository = profRepo;
        }

        [HttpGet("main")]
        public IActionResult MainPanel()
        {
            (string Name, string Surname) personInfo;
            switch (User.GetRole())
            {
                case DeanOfficeRoles.Student:
                    var student = _studentRepository.GetStudent(User.GetGuid());
                    personInfo = (student.FirstName, student.LastName);
                    break;
                case DeanOfficeRoles.Professor:
                    var professor = _professorRepository.GetProfessor(User.GetGuid());
                    personInfo = (professor.FirstName, professor.LastName);
                    break;
                default:
                    personInfo = ("Unknown", "Unknown");
                    break;
            }

            var model = new CourseDashboardViewModel()
            {
                UserRole = User.GetRole(),
                Name = personInfo.Name,
                Surname = personInfo.Surname
            };

            return View(model);
        }

        [HttpGet("schedule")]
        public IActionResult GetSchedule()
        {
            var courses = _courses.GetCoursesFor(User.GetRole(), User.GetGuid());
            var mappedCourses = _mapper.Map<IEnumerable<CourseDto>>(courses);
            var model = new CourseDashboardViewModel()
            {
                UserRole = User.GetRole(),
                Courses = mappedCourses
            };

            return View(model);
        }

        [HttpGet("courses")]
        public IActionResult GetAllCourses()
        {
            var courses = _courses.GetAllCourses();
            var mappedCourses = _mapper.Map<IEnumerable<CourseDto>>(courses);
            var model = new AllCoursesViewModel
            {
                Courses = mappedCourses
            };

            return View(model);
        }

        [HttpGet("course/{courseId:guid}/details")]
        public IActionResult CourseDetails(Guid courseId, [FromQuery]string returnUrl)
        {
            var course = _courses.GetCourse(courseId);
            var mappedCourse = _mapper.Map<CourseDtoWithAttendants>(course);

            var model = new CourseDetailsViewModel
            {
                Course = mappedCourse,
                IsPersonSignedUp = mappedCourse.IsPersonSignedForCourse(User.GetGuid()),
                IsStudent = User.IsStudent(),
                HasAdminRights = User.HasAdminRights(),
                ReturnUrl = returnUrl
            };

            return View(model);
        }
    }
}