﻿using AutoMapper;
using DeanOffice.Data.DatabaseEntities;
using DeanOffice.Models.DataTransferObjects.Course;
using DeanOffice.Models.DataTransferObjects.Professor;
using DeanOffice.Models.DataTransferObjects.Subject;
using DeanOffice.Models.ViewModels.Course;
using DeanOffice.Repository.Interfaces;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using DeanOffice.Models.ViewModels.Error;

namespace DeanOffice.Application.Controllers
{
    [Authorize(Policy = "DeanOfficer")]
    [Route("courses")]
    public class CourseController : Controller
    {
        private readonly IProfessorRepository _lecturerRepository;
        private readonly ICourseRepository _courseRepository;
        private readonly ISubjectRepository _subjectRepository;
        private readonly IMapper _mapper;

        public CourseController(ICourseRepository courseRepo, IMapper mapper,
            ISubjectRepository subjectRepo, IProfessorRepository professorRepo)
        {
            _lecturerRepository = professorRepo;
            _courseRepository = courseRepo;
            _subjectRepository = subjectRepo;
            _mapper = mapper;
        }

        [HttpGet("add")]
        public IActionResult Add()
        {
            var model = new CourseEditViewModel()
            {
                Course = new CourseDto(),
            };
            SetPropertiesOnCourseEditViewModel(model);

            ViewData["Title"] = "Add course";
            return View("AddEdit", model);
        }

        [HttpPost("add/{courseId:guid}")]
        [ValidateAntiForgeryToken]
        public IActionResult Add(Guid courseId, [FromQuery]string returnUrl, [FromBody]CourseEditViewModel model)
        {
            if (courseId != Guid.Empty)
            {
                var errorModel = new ErrorViewModel
                {
                    ErrorMessage = "Course already in database",
                    ReturnUrl = returnUrl
                };
                return View("Error", errorModel);
            }

            if (ModelState.IsValid)
            {
                var course = _mapper.Map<Course>(model.Course);
                _courseRepository.AddCourse(course);
                return RedirectToAction("GetAllCourses", "Dashboard");
            }

            SetPropertiesOnCourseEditViewModel(model);

            ViewData["Title"] = "Add course";
            return View("AddEdit", model);
        }

        [HttpGet("edit/{courseId:guid}")]
        public IActionResult Edit(Guid courseId, [FromQuery]string returnUrl)
        {
            var course = _courseRepository.GetCourse(courseId);
            if (course == null)
            {
                var errorModel = new ErrorViewModel
                {
                    ErrorMessage = "Course couldn't be found",
                    ReturnUrl = returnUrl
                };
                return View("Error", errorModel);
            }

            var mappedCourse = _mapper.Map<CourseDto>(course);

            var model = new CourseEditViewModel
            {
                Course = mappedCourse
            };

            SetPropertiesOnCourseEditViewModel(model);

            ViewData["Title"] = "Edit course";
            return View("AddEdit", model);
        }

        [HttpPost("edit/{courseId:guid}")]
        [ValidateAntiForgeryToken]
        public IActionResult Edit(Guid courseId, CourseEditViewModel model)
        {
            if (!ModelState.IsValid)
            {
                SetPropertiesOnCourseEditViewModel(model);
                ViewData["Title"] = "Edit course";
                return View("AddEdit", model);
            }

            if (model.Course.CurrentStudentCount > model.Course.StudentCountLimit)
            {
                SetPropertiesOnCourseEditViewModel(model);
                ViewData["Title"] = "Edit course";
                ModelState.AddModelError("", "Cannot decrease student count further as students are signed in");
                return View("AddEdit", model);
            }

            var course = _mapper.Map<Course>(model.Course);
            _courseRepository.UpdateCourse(course);
            return RedirectToAction("GetAllCourses", "Dashboard");

        }

        private void SetPropertiesOnCourseEditViewModel(CourseEditViewModel model)
        {
            var subjects = _subjectRepository.GetSubjects();
            var lecturers = _lecturerRepository.GetProfessors();
            var types = _courseRepository.GetCourseTypes();

            var mappedSubjects = _mapper.Map<IEnumerable<SubjectBasicDto>>(subjects);
            var mappedLecturers = _mapper.Map<IEnumerable<ProfessorBasicDto>>(lecturers);
            var mappedTypes = _mapper.Map<IEnumerable<CourseTypeDto>>(types);

            model.SetLecturers(mappedLecturers);
            model.SetSubjects(mappedSubjects);
            model.SetTypes(mappedTypes);
        }
    }
}