﻿using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using DeanOffice.Application.Configuration.Middleware;
using DeanOffice.Application.Configuration.ServiceConfiguration;
using AutoMapper;
using DeanOffice.Mapper.Helpers;

namespace DeanOffice.Application
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddMvc();
            services.AddSingleton(Configuration);
            services.AddDatabaseContext(Configuration);
            services.AddIdentity(Configuration);
            services.ConfigurePasswordPolicy();
            services.AddRepositories();
            services.AddUserValidators();
            services.ConfigureAuthorizationPolicies();
            services.AddAutoMapper(typeof(IAutoMapperProfileAssemblyMarker));
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            app.ConfigureGlobalFlags();

            if (env.IsDevelopment())
            {
                app.UseBrowserLink();
                app.UseDeveloperExceptionPage();
            }

            app.UseStaticFiles();
            app.UseStatusCodePagesWithReExecute("/error");
            app.UseAuthentication();
            app.UseMvc();
        }
    }
}
