﻿using DeanOffice.Data.DataSeeding;
using DeanOffice.Identity;
using Microsoft.AspNetCore;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.DependencyInjection;
using System;

namespace DeanOffice.Application
{
    public class Program
    {
        public static void Main(string[] args)
        {
            var host = BuildWebHost(args);

            using (var scope = host.Services.CreateScope())
            {
                SeedDatabase(scope.ServiceProvider);
            }

            host.Run();
        }

        public static IWebHost BuildWebHost(string[] args) =>
            WebHost.CreateDefaultBuilder(args)
                .UseStartup<Startup>()
                .Build();

        private static void SeedDatabase(IServiceProvider services)
        {
            var identityInitializer = new InitializeIdentity(services);
            identityInitializer.InitializeRootUser();
            var profGuids = identityInitializer.InitializeProfessorUsers();
            var studGuids = identityInitializer.InitializeStudentUsers();

            var dataInitializer = new DeanOfficeDataSeeder(services, profGuids, studGuids);
            dataInitializer.SeedProfessors();
            dataInitializer.SeedStudents();
            dataInitializer.SeedLectureTypes();
            dataInitializer.SeedSubjects();
            dataInitializer.SeedCourses();
        }
    }
}
