﻿using HtmlAgilityPack;
using SKOS.Scraper.NodeParsers;
using Xunit;
using static SKOS.Scraper.NodeSelectors.EmailNodeParser;

namespace SKOS.Scraper.Tests
{
    public class HtmlDetailsNodeSelectorTests
    {
        [Fact]
        public void ShouldReturnMailString_When_HtmlDocumentGiven()
        {
            var html = new HtmlDocument();
            html.Load("TestHtmls/skostest.html");

            var mailNode = html.GetEmailNode();
            var mailStringEnc = GetEncryptedMailString(mailNode);

            Assert.Equal("&gt;a/&lt;lp.ude.hga#kedalozm&gt;\"liame\"=ssalc \"lp.ude.hga#kedalozm:otliam\"=ferh a&lt;",
                mailStringEnc);
        }

        [Fact]
        public void WhenEncryptedMailGiven_DecryptsItCorrecty()
        {
            var question = "&gt;a/&lt;lp.ude.hga#kedalozm&gt;\"liame\"=ssalc \"lp.ude.hga#kedalozm:otliam\"=ferh a&lt;";

            var mail = GetMailFromEncryptedString(question);

            Assert.Equal("mzoladek@agh.edu.pl", mail);
        }
    }
}
