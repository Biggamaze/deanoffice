﻿using HtmlAgilityPack;
using SKOS.Scraper.NodeSelectors;
using Xunit;

namespace SKOS.Scraper.Tests.Selectors
{
    public class NameNodeSelectorTests
    {
        [Fact]
        public void ShouldNotBeNull_When_DetailsDocumentQueriedForNameNode()
        {
            var document = new HtmlDocument();
            document.Load("TestHtmls/skostest.html");

            var nameNode = document.GetFullNameNode();

            Assert.NotNull(nameNode);
        }
    }
}
