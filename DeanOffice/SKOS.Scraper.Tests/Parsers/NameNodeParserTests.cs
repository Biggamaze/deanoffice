﻿using Xunit;
using HtmlAgilityPack;
using SKOS.Scraper.NodeSelectors;
using SKOS.Scraper.Entities.HtmlNodes;
using SKOS.Scraper.NodeParsers;

namespace SKOS.Scraper.Tests.Parsers
{
    public class NameNodeParserTests
    {
        private NameNode FullNameNode { get; }

        public NameNodeParserTests()
        {
            var document = new HtmlDocument();
            document.Load("TestHtmls/skostest.html");
            FullNameNode = document.GetFullNameNode();
        }


        [Fact]
        public void ShouldOutputCorrectName_When_GivenNameNode()
        {
            var name = FullNameNode.GetFirstName();

            Assert.Equal("Maciej", name);
        }

        [Fact]
        public void ShouldOutputCorrectSurname_When_GivenNameNode()
        {
            var name = FullNameNode.GetLastName();

            Assert.Equal("Żołądek", name);
        }
    }
}
