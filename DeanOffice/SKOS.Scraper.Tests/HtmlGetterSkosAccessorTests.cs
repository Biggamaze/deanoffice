﻿using DeanOffice.Common.Enums;
using Xunit;

namespace SKOS.Scraper.Tests
{
    public class HtmlGetterSkosAccessorTests
    {
        [Theory]
        [InlineData("Zoladek", "Maciej", null)]
        public void ExpectedUrlIsGenerated_When_FullDataIsGiven(string lastName, string firstName, TitleEnum? title)
        {
            var getter = new HtmlGetterSkosAccessor();

            var html = getter.GeneratUrl(lastName, firstName, title);

            Assert.Equal($"https://skos.agh.edu.pl/search/?nazwisko=Zoladek&imie=Maciej&tytul=", html);
        }

        [Fact]
        public void HtmlShouldNotBeNull_When_ValidUrlIsGiven()
        {
            var getter = new HtmlGetterSkosAccessor();

            var html = getter.GetPageForSearchResult("Zoladek", "Maciej", TitleEnum.Brak).Result;

            Assert.NotNull(html);
        }
    }
}
