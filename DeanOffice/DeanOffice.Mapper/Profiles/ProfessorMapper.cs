﻿using AutoMapper;
using DeanOffice.Data.DatabaseEntities;
using DeanOffice.Models.DataTransferObjects.Professor;

namespace DeanOffice.Mapper.Profiles
{
    public class ProfessorMapper : Profile
    {
        public ProfessorMapper()
        {
            CreateMap<Professor, ProfessorBasicDto>()
                .ForMember(dest => dest.Id, opts => opts.MapFrom(src => src.UserId));
            CreateMap<ProfessorBasicDto, Professor>()
                .ForMember(dest => dest.UserId, opts => opts.MapFrom(src => src.Id));
        }
    }
}
