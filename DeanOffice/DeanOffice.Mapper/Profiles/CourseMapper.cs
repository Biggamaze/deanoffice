﻿using AutoMapper;
using DeanOffice.Data.DatabaseEntities;
using DeanOffice.Models.DataTransferObjects.Course;
using System.Linq;

namespace DeanOffice.Mapper.Profiles
{
    public class CourseMapper : Profile
    {
        public CourseMapper()
        {
            CreateMap<Course, CourseDto>()
                .ForMember(dest => dest.SubjectId,
                    opts => opts.MapFrom(src => src.Subject.SubjectId))
                .ForMember(dest => dest.Lecturer,
                    opts => opts.MapFrom(src => src.Lecturer))
                .ForMember(dest => dest.CurrentStudentCount,
                    opt => opt.MapFrom(src => src.Students.Count));

            CreateMap<Course, CourseDtoWithAttendants>()
                .IncludeBase<Course, CourseDto>()
                .ForMember(dest => dest.Attendants,
                           opts => opts.MapFrom(src => src.Students.Select(st => st.Student)));

            CreateMap<CourseDto, Course>()
                .ForMember(dest => dest.Lecturer,
                           opts => opts.ResolveUsing((src, dest, course, context) => context.Mapper.Map<Professor>(src.Lecturer)));

            CreateMap<CourseType, CourseTypeDto>().ReverseMap();
        }
    }
}
