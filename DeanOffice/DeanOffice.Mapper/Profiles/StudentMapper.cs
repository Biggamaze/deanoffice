﻿using AutoMapper;
using DeanOffice.Data.DatabaseEntities;
using DeanOffice.Models.DataTransferObjects.Student;

namespace DeanOffice.Mapper.Profiles
{
    public class StudentMapper : Profile
    {
        public StudentMapper()
        {
            CreateMap<Student, StudentBasicDto>()
                .ForMember(dest => dest.Id, opts => opts.MapFrom(src => src.UserId));
            CreateMap<StudentBasicDto, Student>()
                .ForMember(dest => dest.UserId, opts => opts.MapFrom(src => src.Id));
        }
    }
}
