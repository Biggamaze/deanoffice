﻿using AutoMapper;
using DeanOffice.Data.DatabaseEntities;
using DeanOffice.Models.DataTransferObjects.Subject;

namespace DeanOffice.Mapper.Profiles
{
    public class SubjectMapper : Profile
    {
        public SubjectMapper()
        {
            CreateMap<Subject, SubjectBasicDto>().ReverseMap();
        }
    }
}
