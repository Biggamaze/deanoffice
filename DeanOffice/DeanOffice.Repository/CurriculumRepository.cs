﻿using DeanOffice.Data;
using DeanOffice.Data.DatabaseEntities.JoiningEntities;
using DeanOffice.Repository.Interfaces;
using System;

namespace DeanOffice.Repository
{
    public class CurriculumRepository : ICurriculumRepository
    {
        private DeanOfficeEntities _context { get; }

        public CurriculumRepository(DeanOfficeEntities context)
        {
            _context = context;
        }

        public int SignUpForLecture(Guid studentId, Guid courseId)
        {
            if (_context.StudentCourses.Find(courseId, studentId) != null)
            {
                return -1;
            }

            _context.StudentCourses.Add(new StudentLecture { CourseId = courseId, StudentId = studentId });
            return _context.SaveChanges();
        }

        public int SignOutFromLecture(Guid studentId, Guid courseId)
        {
            var entity = new StudentLecture
            {
                CourseId = courseId,
                StudentId = studentId
            };

            _context.StudentCourses.Remove(entity);
            return _context.SaveChanges();
        }
    }
}
