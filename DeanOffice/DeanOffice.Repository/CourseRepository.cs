﻿using System;
using System.Collections.Generic;
using System.Linq;
using DeanOffice.Data;
using DeanOffice.Data.DatabaseEntities;
using DeanOffice.Identity.Models;
using DeanOffice.Repository.Interfaces;
using Microsoft.EntityFrameworkCore;

namespace DeanOffice.Repository
{
    public class CourseRepository : ICourseRepository
    {
        private readonly DeanOfficeEntities _context;
        private static IDictionary<string, Func<Guid, IEnumerable<Course>>> _courseForMethods;

        public CourseRepository(DeanOfficeEntities context)
        {
            _context = context;
            _courseForMethods = new Dictionary<string, Func<Guid, IEnumerable<Course>>>
            {
                { DeanOfficeRoles.Professor, GetCoursesForProfessor },
                { DeanOfficeRoles.Student, GetCoursesForStudent }
            };
        }

        public int AddCourse(Course course)
        {
            _context.Courses.Attach(course);
            return _context.SaveChanges();
        }

        public int UpdateCourse(Course course)
        {
            _context.Courses.Update(course);
            _context.Entry(course.Lecturer).State = EntityState.Unchanged;
            _context.Entry(course.Subject).State = EntityState.Unchanged;
            _context.Entry(course.Type).State = EntityState.Unchanged;
            return _context.SaveChanges();
        }

        public Course GetCourse(Guid courseId)
        {
            return _context.Courses
                .Include(s => s.Subject)
                .Include(s => s.Lecturer)
                .Include(s => s.Type)
                .Include(s => s.Students)
                    .ThenInclude(sc => sc.Student)
                .SingleOrDefault(x => x.CourseId == courseId);
        }

        public IEnumerable<Course> GetAllCourses()
        {
            return _context.Courses
                .Include(s => s.Subject)
                .Include(s => s.Lecturer)
                .Include(s => s.Type)
                .ToList();
        }

        public IEnumerable<Course> GetCoursesFor(string role, Guid id)
        {
            if(_courseForMethods.TryGetValue(role, out Func<Guid, IEnumerable<Course>> method))
            {
                return method(id);
            }
            return Enumerable.Empty<Course>();
        }

        public IEnumerable<CourseType> GetCourseTypes()
        {
            return _context.LectureTypes.ToList();
        }

        private IEnumerable<Course> GetCoursesForStudent(Guid studentId)
        {
            return _context.Courses
                .Where(crs => crs.Students.Any(sc => sc.StudentId == studentId))
                .Include(s => s.Subject)
                .Include(s => s.Type)
                .Include(s => s.Lecturer)
                .ToList();
        }

        private IEnumerable<Course> GetCoursesForProfessor(Guid professorId)
        {
            return _context.Courses
                .Where(crs => crs.Lecturer.UserId == professorId)
                .Include(crs => crs.Students)
                    .ThenInclude(sccol => sccol.Student)
                .Include(crs => crs.Lecturer)
                .Include(s => s.Subject)
                .Include(s => s.Type)
                .ToList();
        }

  
    }
}
