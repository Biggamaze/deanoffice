﻿using DeanOffice.Data;
using DeanOffice.Data.DatabaseEntities;
using DeanOffice.Repository.Interfaces;
using System;

namespace DeanOffice.Repository
{
    public class StudentRepository : IStudentRepository
    {
        private DeanOfficeEntities _context { get; }

        public StudentRepository(DeanOfficeEntities context)
        {
            _context = context;
        }

        public int AddStudent(Student student)
        {
            _context.Students.Add(student);
            return _context.SaveChanges();
        }

        public Student GetStudent(Guid studentId)
        {
            return _context.Students
                .Find(studentId);
        }
    }
}
