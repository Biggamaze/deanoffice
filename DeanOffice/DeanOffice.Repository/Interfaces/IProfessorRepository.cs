﻿using System;
using DeanOffice.Data.DatabaseEntities;
using System.Collections.Generic;

namespace DeanOffice.Repository.Interfaces
{
    public interface IProfessorRepository 
    {
        int AddProfessor(Professor prof);
        Professor GetProfessor(Guid professorId);
        IEnumerable<Professor> GetProfessors();
    }
}
