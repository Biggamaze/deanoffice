﻿using System;

namespace DeanOffice.Repository.Interfaces
{
    public interface ICurriculumRepository
    {
        int SignUpForLecture(Guid studentId, Guid courseId);
        int SignOutFromLecture(Guid studentId, Guid courseId);
    }
}
