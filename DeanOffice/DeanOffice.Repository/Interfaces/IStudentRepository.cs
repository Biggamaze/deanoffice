﻿using DeanOffice.Data.DatabaseEntities;
using System;

namespace DeanOffice.Repository.Interfaces
{
    public interface IStudentRepository 
    {
        Student GetStudent(Guid studentId);
        int AddStudent(Student student);
    }
}
