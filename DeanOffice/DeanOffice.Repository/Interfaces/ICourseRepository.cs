﻿using DeanOffice.Data.DatabaseEntities;
using System;
using System.Collections.Generic;

namespace DeanOffice.Repository.Interfaces
{
    public interface ICourseRepository
    {
        int AddCourse(Course course);
        int UpdateCourse(Course course);
        Course GetCourse(Guid courseId);
        IEnumerable<Course> GetAllCourses();
        IEnumerable<Course> GetCoursesFor(string role, Guid Id);
        IEnumerable<CourseType> GetCourseTypes();
    }
}
