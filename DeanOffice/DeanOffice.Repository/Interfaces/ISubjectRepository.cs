﻿using System;
using DeanOffice.Data.DatabaseEntities;
using System.Collections.Generic;

namespace DeanOffice.Repository.Interfaces
{
    public interface ISubjectRepository
    {
        IEnumerable<Subject> GetSubjects();
        int AddSubject(Subject subject);
        Subject GetSubject(Guid subjectId);
        int UpdateSubject(Subject subject);
    }
}
