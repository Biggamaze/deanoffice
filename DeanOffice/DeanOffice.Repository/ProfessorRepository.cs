﻿using System;
using System.Collections.Generic;
using System.Linq;
using DeanOffice.Data;
using DeanOffice.Data.DatabaseEntities;
using DeanOffice.Repository.Interfaces;

namespace DeanOffice.Repository
{
    public class ProfessorRepository : IProfessorRepository
    {
        public DeanOfficeEntities _context { get; }

        public ProfessorRepository(DeanOfficeEntities context)
        {
            _context = context;
        }

        public int AddProfessor(Professor prof)
        {
            _context.Lecturers.Add(prof);
            return _context.SaveChanges();
        }

        public Professor GetProfessor(Guid professorId)
        {
            return _context.Lecturers.Find(professorId);
        }

        public IEnumerable<Professor> GetProfessors()
        {
            return _context.Lecturers.ToList();
        }
    }
}
