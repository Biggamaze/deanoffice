﻿using System;
using DeanOffice.Data;
using DeanOffice.Data.DatabaseEntities;
using DeanOffice.Repository.Interfaces;
using System.Collections.Generic;
using System.Linq;
using Microsoft.EntityFrameworkCore;

namespace DeanOffice.Repository
{
    public class SubjectRepository : ISubjectRepository
    {
        private readonly DeanOfficeEntities _context;

        public SubjectRepository(DeanOfficeEntities context)
        {
            _context = context;
        }

        public IEnumerable<Subject> GetSubjects()
        {
            return _context.Subjects.ToList();
        }

        public int AddSubject(Subject subject)
        {
            _context.Subjects.Add(subject);
            return _context.SaveChanges();
        }

        public Subject GetSubject(Guid subjectId)
        {
            return _context.Subjects
                .SingleOrDefault(x => x.SubjectId == subjectId);
        }

        public int UpdateSubject(Subject subject)
        {
            _context.Subjects.Update(subject);
            if (subject.Courses != null)
            {
                _context.Entry(subject.Courses).State = EntityState.Unchanged;
            }
            return _context.SaveChanges();
        }
    }
}
