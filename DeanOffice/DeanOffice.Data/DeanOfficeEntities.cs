﻿using DeanOffice.Data.DatabaseEntities;
using DeanOffice.Data.DatabaseEntities.JoiningEntities;
using Microsoft.EntityFrameworkCore;
using System;

namespace DeanOffice.Data
{
    public class DeanOfficeEntities : DbContext
    {
        public DeanOfficeEntities(DbContextOptions<DeanOfficeEntities> options) 
            : base(options)
        {

        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<StudentLecture>().HasKey(t => new { t.CourseId, t.StudentId });
        }

        public DbSet<Student> Students { get; set; }
        public DbSet<Subject> Subjects { get; set; }
        public DbSet<Course> Courses { get; set; }
        public DbSet<CourseType> LectureTypes  { get; set; }
        public DbSet<Professor> Lecturers { get; set; }
        public DbSet<StudentLecture> StudentCourses { get; set; }
    }
}
