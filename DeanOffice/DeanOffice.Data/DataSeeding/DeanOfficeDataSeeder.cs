﻿using DeanOffice.Data.DatabaseEntities;
using DeanOffice.Data.DatabaseEntities.JoiningEntities;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http.Headers;

namespace DeanOffice.Data.DataSeeding
{
    public class DeanOfficeDataSeeder
    {
        private readonly IServiceProvider _serviceProvider;
        private readonly IEnumerable<Guid> _professorGuids;
        private readonly IEnumerable<Guid> _studentGuids;
        private readonly Guid[] _subjectGuids;
        private readonly int[] _typeIds;

        public DeanOfficeDataSeeder(IServiceProvider provider, IEnumerable<Guid> profGuids, IEnumerable<Guid> studGuids)
        {
            _serviceProvider = provider;
            _subjectGuids = new[]
                {
                    Guid.Parse("1f5d7b0d-693e-40b9-87e5-649d65d50e94"),
                    Guid.Parse("6cd0d04f-2773-4edb-abd8-321bf77a8ff5")
                };
            _professorGuids = profGuids;
            _studentGuids = studGuids;
            _typeIds = new[] { 1, 2 };
        }

        public void SeedProfessors()
        {
            var context = _serviceProvider.GetRequiredService<DeanOfficeEntities>();
            {
                int i = 0;
                foreach (var guid in _professorGuids)
                {
                    if (context.Lecturers.Find(guid) != null)
                    {
                        continue;
                    }
                    context.Lecturers.Add(new Professor { UserId = guid, FirstName = "Prof", LastName = i++.ToString() });
                }
                context.SaveChanges();
            }
        }

        public void SeedStudents()
        {
            var context = _serviceProvider.GetRequiredService<DeanOfficeEntities>();
            {
                int i = 0;
                foreach (var guid in _studentGuids)
                {
                    if (context.Students.Find(guid) != null)
                    {
                        continue;
                    }
                    context.Students.Add(new Student { UserId = guid, FirstName = "Student", LastName = i++.ToString() });
                }
                context.SaveChanges();
            }
        }

        public void SeedLectureTypes()
        {
            var context = _serviceProvider.GetRequiredService<DeanOfficeEntities>();
            var lectureTypes = new[]
            {
                new CourseType
                {
                    TypeId = _typeIds[0],
                    Name = "Course",
                    Description = "Fuck pussy hard"
                },
                new CourseType
                {
                    TypeId = _typeIds[1],
                    Name = "Training",
                    Description = ""
                }
            };

            var present = context.LectureTypes.Where(type => _typeIds.Contains(type.TypeId));

            foreach (var type in lectureTypes)
            {
                if (present.Select(pr => pr.TypeId).Contains(type.TypeId))
                {
                    continue;
                }
                context.Add(type);
            }

            context.SaveChanges();
        }

        public void SeedSubjects()
        {
            var context = _serviceProvider.GetRequiredService<DeanOfficeEntities>();
            {
                var subjects = new[]
                {
                    new Subject
                    {
                        SubjectId = _subjectGuids[0],
                        Name = "Biology"
                    },
                    new Subject
                    {
                        SubjectId = _subjectGuids[1],
                        Name = "Anathomy"
                    }
                };

                var present = context.Subjects.Where(sub => _subjectGuids.Contains(sub.SubjectId));
                foreach (var subject in subjects)
                {
                    if (present.Select(pr => pr.SubjectId).Contains(subject.SubjectId))
                    {
                        continue;
                    }
                    context.Add(subject);
                }

                context.SaveChanges();
            }
        }

        public void SeedCourses()
        {
            var context = _serviceProvider.GetRequiredService<DeanOfficeEntities>();
            {
                var courseGuids = new[]
                {
                    Guid.Parse("ea1bd128-99b1-4341-82db-55c7b43e69dc"),
                    Guid.Parse("6c42fe93-d2ec-44f8-8fd7-fcb5368ebd08"),
                    Guid.Parse("9d7cb9f0-fa2c-4871-959a-c5840c536383")
                };

                var students = context.Students
                    .Where(stud => _studentGuids.Contains(stud.UserId))
                    .ToArray();

                var professors = context.Lecturers
                    .Where(prof => _professorGuids.Contains(prof.UserId))
                    .ToArray();

                var subjects = context.Subjects
                    .Where(sub => _subjectGuids.Contains(sub.SubjectId))
                    .ToArray();

                var lectureTypes = context.LectureTypes
                    .Where(lt => _typeIds.Contains(lt.TypeId))
                    .ToArray();

                var courses = new[]
                {
                    new Course
                    {
                        CourseId = courseGuids[0],
                        StartupDate = new DateTime(2017, 1, 1),
                        FinishDate = new DateTime(2018, 1, 1),
                        StartHour = TimeSpan.FromHours(12),
                        EndHour = TimeSpan.FromHours(13),
                        Lecturer = professors[0],
                        Name = "Methodology in biology",
                        Subject = subjects[0],
                        DayOfWeek = DayOfWeek.Saturday,
                        Type = lectureTypes[0],
                        StudentCountLimit = 15
                    },
                    new Course
                    {
                        CourseId = courseGuids[1],
                        StartupDate = new DateTime(2017, 1, 1),
                        FinishDate = new DateTime(2018, 1, 1),
                        StartHour = TimeSpan.FromHours(12),
                        EndHour = TimeSpan.FromHours(13),
                        Lecturer = professors[1],
                        Name = "Methodology in biology II",
                        Subject = subjects[0],
                        DayOfWeek = DayOfWeek.Tuesday,
                        Type = lectureTypes[1],
                        StudentCountLimit = 10
                    },
                    new Course
                    {
                        CourseId = courseGuids[2],
                        StartupDate = new DateTime(2017, 1, 1),
                        FinishDate = new DateTime(2018, 1, 1),
                        StartHour = TimeSpan.FromHours(12),
                        EndHour = TimeSpan.FromHours(13),
                        Lecturer = professors[2],
                        Name = "About little cats",
                        Subject = subjects[1],
                        DayOfWeek = DayOfWeek.Tuesday,
                        Type = lectureTypes[0],
                        StudentCountLimit = 5
                    }
                };

                foreach (var course in courses)
                {
                    course.Students = new List<StudentLecture>()
                    {
                        new StudentLecture
                        {
                            CourseId = course.CourseId,
                            Course = course,
                            StudentId = students[0].UserId,
                            Student = students[0]
                        },
                        new StudentLecture
                        {
                            CourseId = course.CourseId,
                            Course = course,
                            StudentId = students[1].UserId,
                            Student = students[1]
                        },
                        new StudentLecture
                        {
                            CourseId = course.CourseId,
                            Course = course,
                            StudentId = students[2].UserId,
                            Student = students[2]
                        }
                    };
                }

                foreach (var course in courses)
                {
                    if (context.Courses.Find(course.CourseId) != null)
                    {
                        continue;
                    };
                    context.Add(course);
                }
                context.SaveChanges();
            }
        }

    }
}
