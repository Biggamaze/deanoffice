﻿using System;
using System.Collections.Generic;

namespace DeanOffice.Data.DatabaseEntities
{
    public class Subject
    {
        public Guid SubjectId { get; set; }
        public string Name { get; set; }

        public ICollection<Course> Courses { get; set; }
    }
}
