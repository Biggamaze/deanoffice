﻿using System;

namespace DeanOffice.Data.DatabaseEntities.JoiningEntities
{
    public class StudentLecture
    {
        public Guid StudentId { get; set; }
        public Student Student { get; set; }

        public Guid CourseId { get; set; }
        public Course Course { get; set; }
    }
}
