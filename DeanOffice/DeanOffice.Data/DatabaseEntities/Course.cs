﻿using DeanOffice.Data.DatabaseEntities.JoiningEntities;
using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;

namespace DeanOffice.Data.DatabaseEntities
{
    public class Course 
    {
        public Guid CourseId { get; set; }
        public string Name { get; set; }
        public Professor Lecturer { get; set; }
        public Subject Subject { get; set; }
        public DayOfWeek DayOfWeek { get; set; }
        public CourseType Type { get; set; }
        public TimeSpan StartHour { get; set; }
        public TimeSpan EndHour { get; set; }
        public DateTime StartupDate { get; set; }
        public DateTime FinishDate { get; set; }
        public int StudentCountLimit { get; set; }

        public ICollection<StudentLecture> Students { get; set; } = new List<StudentLecture>();
    }
}
