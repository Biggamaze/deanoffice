﻿using DeanOffice.Data.DatabaseEntities.JoiningEntities;
using System.ComponentModel.DataAnnotations.Schema;
using System.Collections.Generic;
using System;
using System.ComponentModel.DataAnnotations;

namespace DeanOffice.Data.DatabaseEntities
{
    public class Student
    {
        [Key]
        public Guid UserId { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }

        public ICollection<StudentLecture> StudentCourses { get; set; }

        [NotMapped]
        public string FullName => $"{FirstName} {LastName}";
    }
}
