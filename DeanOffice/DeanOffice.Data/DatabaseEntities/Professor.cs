﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace DeanOffice.Data.DatabaseEntities
{
    public class Professor
    {
        [Key]
        public Guid UserId { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }

        public ICollection<Course> Courses { get; set; }
    }
}
