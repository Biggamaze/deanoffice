﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace DeanOffice.Data.DatabaseEntities
{
    public class CourseType
    {
        [Key, DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int TypeId { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }

        public ICollection<Course> Courses { get; set; }
    }
}
